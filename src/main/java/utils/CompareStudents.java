package utils;

import institution.University;
import person.Student;

import java.util.ArrayList;
import java.util.List;

public class CompareStudents {

    public List<Student> addToIntern(University university) {
        List<Student> studentList = new ArrayList<>();
        int sumKnowledge = 0;
        int averageValue;
        for (Student student : university.getStudentList()) {
            sumKnowledge += student.getKnowledge().getLevel();
        }
        averageValue = sumKnowledge / university.getStudentList().size();
        for (Student student : university.getStudentList()) {
            if (student.getKnowledge().getLevel() > averageValue) {
                studentList.add(student);
            }
        }
        return studentList;
    }
}
