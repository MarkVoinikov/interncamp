package institution.interlink;


import institution.University;
import person.Student;
import utils.CompareStudents;

import java.util.ArrayList;
import java.util.List;

public class Internship {
    private String name;
    private Student student;

    public Internship(String name) {
        this.name = name;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getStudents(University university) {
        CompareStudents compareStudents = new CompareStudents();
        return compareStudents.addToIntern(university).toString();
    }
}
